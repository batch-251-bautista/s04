# [SECTION] Encapsulation
class Person():
	def __init__(self):
		self._name = "John Doe"
		self._age = 0

	# setter methods
	def set_name(self, name):
		self._name = name

	# getter methods
	def get_name(self):
		print(f"Name of Person: {self._name}")

	def get_age(self):
		print(f"Age of Person: {self._age}")


person1 = Person()

person1.get_name()
person1.set_name("Elon Musk")
person1.get_name()
person1.get_age()


# [SECTION] Inheritance
class Employee(Person):
	def __init__(self, employeeId):
		super().__init__() # Gives access to the properties of the parent class (which is the Person class)
		self._employeeId = employeeId

	# getter method
	def get_employeeId(self):
		print(f"The employeeId is {self._employeeId}")

	# setter method
	def set_employeeId(self, employeeId):
		self._employeeId = employeeId

	# details method
	def get_details(self):
		print(f"{self._employeeId} belongs to {self._name}")

employee1 = Employee("EMP-001")
employee1.get_details()
employee1.set_name("Bob Dylan")
employee1.get_details()


# [SECTION] Polymorphism
# Polymorphism pertains to the occurence where that same function can be used for two or more different classes due to them having the similar functions
class TeamLead():
	def occupation(self):
		print("Team Lead")

	def hasAuth(self):
		print(True)

class TeamMember():
	def occupation(self):
		print("Team Member")

	def hasAuth(self):
		print(False)

team_lead = TeamLead()
team_member = TeamMember()

for person in (team_lead, team_member):
	person.occupation()
	person.hasAuth()


# [SECTION] Abstraction
# Abstraction is when you import a class or a function into your current file and are able to use their functionalities without the actual code of those classes/functions actually being in your current file
from abc import ABC, abstractclassmethod

class Polygon(ABC):
	@abstractclassmethod
	def printNumberOfSides(self):
		# The 'pass' keyword denotes that the method doesn't do anything
		pass

class Triangle(Polygon):
	def __init__(self):
		super().__init__()

	def printNumberOfSides(self):
		print(f"This polygon has 3 sides")

shape = Triangle()
shape.printNumberOfSides()