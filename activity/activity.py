# 1. Create an abstract class called Animal that has the following abstract methods
# eat(food)
# make_sound()
from abc import ABC, abstractclassmethod

class Animal(ABC):
	@abstractclassmethod
	def eat(self, food):
		pass

	@abstractclassmethod
	def make_sound(self):
		pass


# 2. Create two classes that implements the Animal class called Cat and Dog with each of the following properties and methods
# properties
	# Name
	# Breed
	# Age
# methods
	# getters
	# setters
	# implementation of abstract methods
	# call()

class Dog(Animal):
	def __init__(self, name, breed, age):
		super().__init__()
		self._name = name
		self._breed = breed
		self._age = age

	def set_name(self, name):
		self._name = name

	def get_name(self):
		return self._name

	def set_breed(self, breed):
		self._breed = breed

	def get_breed(self):
		return self._breed

	def set_age(self, age):
		self._age = age

	def get_age(self):
		return self._age

	def eat(self, food):
		print(f"Eaten {food}")

	def make_sound(self):
		print(f"Bark! Woof! Arf!")

	def call(self):
		print(f"Here {self.get_name()}!")

dog1 = Dog("Isis", "Dalmatian", 15)
dog1.eat("Steak")
dog1.make_sound()
dog1.call()


class Cat(Animal):
	def __init__(self, name, breed, age):
		super().__init__()
		self._name = name
		self._breed = breed
		self._age = age

	def set_name(self, name):
		self._name = name

	def get_name(self):
		return self._name

	def set_breed(self, breed):
		self._breed = breed

	def get_breed(self):
		return self._breed

	def set_age(self, age):
		self._age = age

	def get_age(self):
		return self._age

	def eat(self, food):
		print(f"Serve me {food}")

	def make_sound(self):
		print("Miaow! Nyaw! Nyaaaaa!")

	def call(self):
		print(f"{self.get_name()}, come on!")

cat1 = Cat("Puss", "Persian", 4)
cat1.eat("Tuna")
cat1.make_sound()
cat1.call()